/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LOGICA;

import DATOS.CLIENTE_BE;
import java.util.ArrayList;

/**
 *
 * @author TONY DC
 */
public class L_CLIENTE_BE {
    
    public static ArrayList Lcliente = new ArrayList();
    public static int estado;
    private conexion2 con = new conexion2();
    
    /*INSERTAR*/
   public void insertar(CLIENTE_BE temp){
        try {
            con.getSt().executeUpdate("INSERT into CLIENTE values('"+temp.getNombre()+
                                                                    "','"+temp.getApellido()+
                                                                    "','"+temp.getDocumento()+
                                                                    "','"+temp.getNumero()+
                                                                    "','"+temp.getNacionalidad()+
                                                                    "','"+temp.getDireccion()+
                                                                    "','"+temp.getTelefono()+
                                                                    "','"+temp.getCorreo()+"')");
        } catch (Exception e) {
        }
    }
     /*ELIMINAR*/
   public void eliminar(CLIENTE_BE temp){
        try {
            con.getSt().executeUpdate("DELETE FROM CLIENTE WHERE NUM_DOC='"+temp.getNumero()+"'");
        } catch (Exception e) {
        }
    }
     /*ACTUALIZAR*/
   public void actualizar(CLIENTE_BE temp){
        try {
            con.getSt().executeUpdate("UPDATE CLIENTE SET NOM_CLI='"+temp.getNombre()+"',"
                                                                + "APE_CLI='"+temp.getApellido()+"',"
                                                                + "ID_TIP_DOC='"+temp.getDocumento()+"',"
                                                                + "ID_NAC='"+temp.getNacionalidad()+"',"
                                                                + "DIR_CLI='"+temp.getDireccion()+"',"
                                                                + "TEL_CLI='"+temp.getTelefono()+"',"
                                                                + "EMA_CLI='"+temp.getCorreo()+"'"             
                                                                + "where NUM_DOC='"+temp.getNumero()+"'");
        } catch (Exception e) {
        }
    }
   
    /*CONSULTAR*/
    public void consultar(){
        con.consulta("SELECT * FROM V_CLIENTE");
        Lcliente.clear();
        try {
            while (con.getRs().next()){
                CLIENTE_BE temp=new CLIENTE_BE(
                        con.getRs().getString(1),
                        con.getRs().getString(2), 
                        con.getRs().getString(3), 
                        con.getRs().getString(4), 
                        con.getRs().getString(5), 
                        con.getRs().getString(6), 
                        con.getRs().getString(7),  
                        con.getRs().getString(8),  
                        con.getRs().getString(9)); 
                Lcliente.add(temp);
            }
        } catch (Exception e) {}
    } 
   
    
}
