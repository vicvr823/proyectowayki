package LOGICA;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author TONY DC
 */
public class conexion2 {
    
     private Connection con;
    private Statement st;
    private ResultSet rs; 
    private String url;

    public conexion2() {
        url="jdbc:sqlserver://waikiserver.database.windows.net:1433;database=FINAL;user=waikiad@waikiserver;password=Valencia4510811;encrypt=true;trustServerCertificate=false;hostNameInCertificate=*.database.windows.net;loginTimeout=30;";
        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            con=DriverManager.getConnection(url,"waikiad","Valencia4510811");
            st=con.createStatement();
            System.out.println("Exito");
        } catch (Exception e) {
            System.out.println("Fracaso");
        }
    }
    
    public conexion2(String url,String sa, String pass, String dri) {
        this.url=url;
        try {
            Class.forName(dri);
            con=DriverManager.getConnection(url,sa,pass);
            st=con.createStatement();
            System.out.println("Exito");
        } catch (Exception e) {
            System.out.println("Fracaso");
        }
    }
    
    //Metodo para recibir cualquier consulta
    public void consulta(String qry){
        try {
            rs=st.executeQuery(qry);
        } catch (Exception e) {
        }
    }
    
    public conexion2(String url) {
        this.url = url;
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }

    public Statement getSt() {
        return st;
    }

    public void setSt(Statement st) {
        this.st = st;
    }

    public ResultSet getRs() {
        return rs;
    }

    public void setRs(ResultSet rs) {
        this.rs = rs;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "conexion{" + "con=" + con + ", st=" + st + ", rs=" + rs + ", url=" + url + '}';
    }
    
    
}
