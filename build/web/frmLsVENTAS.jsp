<%@page import="DATOS.VENTAS_BE"%>
<%@page import="LOGICA.L_VENTAS_BE"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Proyecto</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="recursos/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="recursos/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="recursos/bower_components/Ionicons/css/ionicons.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="recursos/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="recursos/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="recursos/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="recursos/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="recursos/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="recursos/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="recursos/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="recursos/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="index.jsp" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>W</b>aiky</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Proyecto</b>Waiky</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="recursos/img/pp.jpg" class="user-image" alt="User Image">
              <span class="hidden-xs">Tony DC</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                  <img src="recursos/img/pp.jpg" class="img-circle" alt="User Image">

                <p>
                  Juan Anthony Diaz - Florero Web
                  <small>PASAR NO ES IGUAL A APRENDER</small>
                </p>
              </li>
              <!-- Menu Body -->
              
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Cerrar Sesion</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
            <img src="recursos/img/uno.jpeg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Waiki Golosinas</p>
          
        </div>
      </div>
      <!-- search form -->
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
          <li class="header"><b>MANTENIMIENTO</b></li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-group "></i> <span>Modulo Ventas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="Clientes.jsp"><i class="fa fa-circle-o"></i> Mantener Cliente</a></li>
              <li><a href="Ventas.jsp"><i class="fa fa-circle-o"></i> Ventas</a></li>
              <li><a href="Detalle_Venta.jsp"><i class="fa fa-circle-o"></i> Detalle Ventas</a></li>
          </ul>
        </li>
        
        
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i>
            <span>Trabajador</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="Trabajador.jsp"><i class="fa fa-circle-o"></i> Mantener Trabajador</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Modulo Almacen</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             <li class="active"><a href="Articulos.jsp"><i class="fa fa-circle-o"></i> Articulos</a></li>
             <li><a href="Stock.jsp"><i class="fa fa-circle-o"></i> Stock</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-car"></i> <span>Modulo Compras</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li><a href="Proveedor.jsp"><i class="fa fa-circle-o"></i> Mantener Proveedor</a></li>
              <li><a href="Ingresos.jsp"><i class="fa fa-circle-o"></i> Ingresos</a></li>
              <li><a href="Detalle_Ingreso.jsp"><i class="fa fa-circle-o"></i> Detalle Ingresos</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background-image: url(recursos/img/fondo4.png)">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mantenimiento Venta
        <small>Interfaz</small>
      </h1>
      <ol class="breadcrumb">
          <li><a href="index.jsp"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Presentacion</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Lista Ventas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form name="frminicio" action="index.jsp">
            
            <%
                if(request.getParameter("btnGuardar")!=null){
                    L_VENTAS_BE.estado=1;
                }
                if(request.getParameter("btnEliminar")!=null){
                    L_VENTAS_BE.estado=2;
                }
                if(request.getParameter("btnModificar")!=null){
                    L_VENTAS_BE.estado=3;
                }
            %>
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>CODIGO</th>
                        <th>SERIE</th>
                        <th>NUMERO</th>
                        <th>CLIENTE</th>
                        <th>TRABAJADOR</th>
                        <th>FECHA</th>

                    </tr>
                </thead>
                <tbody>
                    <%
                        VENTAS_BE temp=new VENTAS_BE(); //La variable region recibe los datos desde afuera
                        L_VENTAS_BE con=new L_VENTAS_BE();//Con esto se establace la conexion a la base de datos
                        if (L_VENTAS_BE.estado==1){
                            temp=new VENTAS_BE(request.getParameter("txt1")
                                    ,request.getParameter("txt2")
                                    ,request.getParameter("txt3")
                                    ,request.getParameter("txt4")
                                    ,request.getParameter("txt5")
                                    ,request.getParameter("txt6"));
                            con.insertar(temp);
                        }
                        if (L_VENTAS_BE.estado==2){
                            temp.setNumero(request.getParameter("txt4"));
                            con.eliminar(temp);
                        }
                        if (L_VENTAS_BE.estado==3){
                            temp=new VENTAS_BE(request.getParameter("txt1")
                                     ,request.getParameter("txt2")
                                    ,request.getParameter("txt3")
                                    ,request.getParameter("txt4")
                                    ,request.getParameter("txt5")
                                    ,request.getParameter("txt6"));
                            con.actualizar(temp);
                        }
                        con.consultar();
                        for (int  i = 0;  i < L_VENTAS_BE.Lventas.size();  i++) {
                                temp=(VENTAS_BE)L_VENTAS_BE.Lventas.get(i);
                    %>
                    <tr>
                        <td><%=temp.getCodigo()%></td>
                        <td><%=temp.getSerie()%></td>                        
                        <td><%=temp.getNumero()%></td>                        
                        <td><%=temp.getCliente()%></td>                        
                        <td><%=temp.getTrabajador()%></td>                        
                        <td><%=temp.getFecha()%></td>                                               
                    </tr>
                    <% }%>
                </tbody>
              </table>
                </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
    </section>
    <!-- /.content -->
  </div>
  
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>UPN</b> 2020-II
    </div>
    <strong>Copyright &copy; 2020 <a href="">Equipo UPN</a>.</strong> Tdos los derechos
    reservados.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
          
      </div>
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="recursos/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="recursos/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="recursos/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="recursos/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="recursos/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="recursos/bower_components/raphael/raphael.min.js"></script>
<script src="recursos/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="recursos/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="recursos/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="recursos/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="recursos/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="recursos/bower_components/moment/min/moment.min.js"></script>
<script src="recursos/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="recursos/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="recursos/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="recursos/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="recursos/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="recursos/js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="recursos/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="recursos/js/demo.js"></script>

<script>
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'paging'      : true,
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true,
      'autoWidth'   : false
    })
  })
</script>
</body>
</html>
